/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Video from 'react-native-video';
import {TextTrackType} from 'react-native-video';
import Home from './Home';

const Section = ({children, title}): Node => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View>
      <Text>Example Rendering Video</Text>
      <View>
        <Video
          source={{
            uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
            mainVer: 1,
            patchVer: 0,
          }}
          resizeMode={'cover'}
          style={{width: 800, height: 800}}
          onLoad={{
            canPlaySlowForward: true,
            canPlayReverse: true,
            canPlaySlowReverse: true,
            canPlayFastForward: true,
            canStepForward: true,
            canStepBackward: true,
            currentTime: 0,
            duration: 5910.208984375,
            naturalSize: {
              height: 1080,
              orientation: 'landscape',
              width: '1920',
            },
            audioTracks: [
              {language: 'es', title: 'Spanish', type: 'audio/mpeg', index: 0},
              {language: 'en', title: 'English', type: 'audio/mpeg', index: 1},
            ],
            textTracks: [
              {title: '#1 French', language: 'fr', index: 0, type: 'text/vtt'},
              {
                title: '#2 English CC',
                language: 'en',
                index: 1,
                type: 'text/vtt',
              },
              {
                title: '#3 English Director Commentary',
                language: 'en',
                index: 2,
                type: 'text/vtt',
              },
            ],
            videoTracks: [
              {
                bitrate: 3987904,
                codecs: 'avc1.640028',
                height: 720,
                trackId: 'f1-v1-x3',
                width: 1280,
              },
              {
                bitrate: 7981888,
                codecs: 'avc1.640028',
                height: 1080,
                trackId: 'f2-v1-x3',
                width: 1920,
              },
              {
                bitrate: 1994979,
                codecs: 'avc1.4d401f',
                height: 480,
                trackId: 'f3-v1-x3',
                width: 848,
              },
            ],
          }}
          textTracks={[
            {
              title: 'English CC',
              language: 'en',
              type: TextTrackType.VTT, // "text/vtt"
              uri: 'https://bitdash-a.akamaihd.net/content/sintel/subtitles/subtitles_en.vtt',
            },
            {
              title: 'Spanish Subtitles',
              language: 'es',
              type: TextTrackType.SRT, // "application/x-subrip"
              uri: 'https://durian.blender.org/wp-content/content/subtitles/sintel_es.srt',
            },
          ]}
        />
      </View>
    </View>
  );
};

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <View>
      <Text>Example Rendering Video</Text>
      <View>
        <Home />
        {/* <Video
          source={{
            uri: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4',
          }}
          style={{width: 300, height: 300}}
        /> */}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
